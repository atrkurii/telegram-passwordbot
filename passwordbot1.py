#!/usr/bin/python3

# This bot creates CSV files in the same folder, from where it runs on my pc with all passwords of every user,
# files are named after users' telegram id




import jsonyaml_converter


from telegram import ReplyKeyboardMarkup
from telegram.ext import (
    Updater,
    CommandHandler,
    MessageHandler,
    Filters,
    ConversationHandler,
)

from password_generator import PasswordGenerator

from enum import Enum


service = ""
login = ""
storage = jsonyaml_converter.init_storage()
storage.new()


class ConvHandlerState(Enum):
    NAME = 0
    LOGIN = 1
    LENGTH = 2
    PRINT_PWD = 3
    DELETE = 4
    ADDNAME = 5
    ADDLOGIN = 6
    PASSWORD = 7


def initialize_user(update, context):
    reply_keyboard = [
        ["/generate_password", "/add_password", "/all_services"],
        ["/delete_password", "/show_password", "/help"],
    ]
    update.message.reply_text(
        "Hi, I am a password BOT, \n"
        "I can create, store and organize passwords for you. ( ͡ᵔ ͜ʖ ͡ᵔ )\n"
        "Type /help to get an idea how everything works",
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True),
    )


def gen_pass(update, context):
    update.message.reply_text("Enter name of the service.")
    return ConvHandlerState.NAME


def ask_login(update, context):
    global service
    service = update.message.text
    update.message.reply_text("Enter your login.")
    return ConvHandlerState.LOGIN


def ask_pwd_len(update, context):
    global login
    login = update.message.text
    update.message.reply_text("Enter length of the password.")
    return ConvHandlerState.LENGTH


def return_pwd(update, context):
    length = update.message.text
    if not length.isnumeric():
        update.message.reply_text("I need numbers!\n" "Try typing number this time.")
        pass
    else:
        pwd_len = int(length)
        pwo = PasswordGenerator()
        pwo.minlen = pwd_len
        pwo.maxlen = pwd_len
        pwo.excludeschars = "!$%^,.=+|/<>"
        password = pwo.generate()
        storage.put(update.message.chat_id, service_name = service, login = login, password = password)
        update.message.reply_text(f'your new password is: "{password}"!')
        return ConversationHandler.END


def add_pass(update, context):
    update.message.reply_text("Enter name of the service.")
    return ConvHandlerState.ADDNAME


def ask_login_to_add(update, context):
    global service
    service = update.message.text
    update.message.reply_text("Enter your login.")
    return ConvHandlerState.ADDLOGIN


def ask_pwd_to_add(update, context):
    global login
    login = update.message.text
    update.message.reply_text("Enter your password.")
    return ConvHandlerState.PASSWORD


def save_pwd_to_add(update, context):
    storage.put(update.message.chat_id, service_name=service, login=login, password=update.message.text)
    update.message.reply_text(
        "Your password is saved and can be acessed with /show_password."
    )
    return ConversationHandler.END


def print_all(update, context):
    output = ""
    usrdata = storage.get(update.message.chat_id)
    if usrdata:
        for key in usrdata:
            output += f"\n index = {key}, service = {usrdata[key]['service_name']}"
        update.message.reply_text(output)
    else:
        update.message.reply_text("There are no services yet.")


def getpass(update, context):
    update.message.reply_text("Enter index of desired service.")
    return ConvHandlerState.PRINT_PWD


def print_pwd(update, context):
    usrdata = storage.get(update.message.chat_id)
    if usrdata:
        password = ""
        index = update.message.text
        if not index.isnumeric():
            update.message.reply_text("I need numbers!\n" "Try typing number this time.")
            pass
        else:
            if index in usrdata:
                password = usrdata[index]['password']
            else:
                update.message.reply_text("There is no such index.")
                return ConversationHandler.END
            update.message.reply_text(password)
            return ConversationHandler.END
    else:
        update.message.reply_text("There are no services yet.")
        return ConversationHandler.END


def delete_item(update, context):
    usrdata = storage.get(update.message.chat_id)
    if usrdata:
        update.message.reply_text("Enter index of service to be deleted: ")
        return ConvHandlerState.DELETE
    else:
        update.message.reply_text("There are no services yet.")
        return ConversationHandler.END


def delete(update, context):
    usrdata = storage.get(update.message.chat_id)
    index = update.message.text
    if not index.isnumeric():
        update.message.reply_text("I need numbers!\n" "Try typing number this time.")
        pass
    else:
        if str(index) in usrdata:
            storage.delete(update.message.chat_id, pwd_id=str(index))
            update.message.reply_text("Selected password is deleted.")
            return ConversationHandler.END
        else:
            update.message.reply_text("There is no such index.")
            return ConversationHandler.END


def stop():
    return ConversationHandler.END


def erase(update, context):
    storage.delete_all(update.message.chat_id)


def help_comand(update, context):
    update.message.reply_text(
        f"This bot creates, stores, organizes passwords for you. \n"
        "/star creates or erases database with all your passwords.\n"
        "To generate password type /generate_password or tap corresponding button. \n"
        "To add existing password type /add_password or tap corresponding button. \n"
        "To view all services you saved type /all_services or tap corresponding button. \n"
        "To get a password already created type /show_password or tap corresponding button. \n"
        "To delete an existing password type /delete_password or tap corresponding button. \n"
    )


def main():
    updater = Updater(
        token=jsonyaml_converter.config()['token'], use_context=True
    )
    dp = updater.dispatcher

    dp.add_handler(CommandHandler("start", initialize_user))
    dp.add_handler(CommandHandler("all_services", print_all))
    dp.add_handler(CommandHandler("help", help_comand))
    dp.add_handler(CommandHandler("erase_all_data", erase))

    conv_handler = ConversationHandler(
        entry_points=[
            CommandHandler("generate_password", gen_pass),
            CommandHandler("show_password", getpass),
            CommandHandler("delete_password", delete_item),
            CommandHandler("add_password", add_pass),
        ],
        states={
            ConvHandlerState.NAME: [
                MessageHandler(Filters.text, ask_login, pass_user_data=True)
            ],
            ConvHandlerState.LOGIN: [
                MessageHandler(Filters.text, ask_pwd_len, pass_user_data=True)
            ],
            ConvHandlerState.LENGTH: [
                MessageHandler(Filters.text, return_pwd, pass_user_data=True)
            ],
            ConvHandlerState.PRINT_PWD: [
                MessageHandler(Filters.text, print_pwd, pass_user_data=True)
            ],
            ConvHandlerState.DELETE: [
                MessageHandler(Filters.text, delete, pass_user_data=True)
            ],
            ConvHandlerState.ADDNAME: [
                MessageHandler(Filters.text, ask_login_to_add, pass_user_data=True)
            ],
            ConvHandlerState.ADDLOGIN: [
                MessageHandler(Filters.text, ask_pwd_to_add, pass_user_data=True)
            ],
            ConvHandlerState.PASSWORD: [
                MessageHandler(Filters.text, save_pwd_to_add, pass_user_data=True)
            ],
        },
        fallbacks=[CommandHandler("stop", stop)],
    )

    dp.add_handler(conv_handler)

    updater.start_polling()

    updater.idle()


if __name__ == "__main__":
    main()
