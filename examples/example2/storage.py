from dataclasses import dataclass, asdict
import typing as t
import json
import yaml
import os

@dataclass
class TraceRoute:
    host: str
    loss: float
    avg: float
    best: float
    worst: float
    dev: float

class JSONDecoder(json.JSONDecoder):
    def __init__(self, *args, **kwargs):
        json.JSONDecoder.__init__(self, object_hook=self.object_hook, *args, **kwargs)

    def object_hook(self, obj):
        if '_type' not in obj:
            return obj
        this_type = obj['_type']
        if this_type == 'TraceRoute':
            del obj['_type']
            return TraceRoute(**obj)
        return obj

class JSONEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, TraceRoute):
            item = asdict(obj)
            item['_type'] = "TraceRoute"
            return item
        return json.JSONEncoder.default(self, obj)

def encode_json(data):
    return json.dumps(data, cls=JSONEncoder, indent=2)

def decode_json(data):
    return json.loads(data, cls=JSONDecoder)

def decode_yaml(data):
    return yaml.load(data)

supported_formats = {
    "from": {
        ".yaml": decode_yaml,
        ".json": decode_json
    },
    "to": {
        ".yaml": yaml.dump,
        ".json": encode_json
    }
}

@dataclass
class TraceRoutes:
    results: t.List[TraceRoute]
    storage_file: t.AnyStr

    def to_file(self):
        path, ext = os.path.splitext(self.storage_file)
        try:
            format_function = supported_formats["to"][ext]
        except KeyError as ke:
            raise RuntimeError(f"Storage '{ext}'format is not supported")

        with open(self.storage_file, "w") as storage:
            storage.write(format_function(self.results))

    def from_file(self):
        path, ext = os.path.splitext(self.storage_file)
        try:
            format_function = supported_formats["from"][ext]
        except KeyError as ke:
            raise RuntimeError(f"Storage '{ext}'format is not supported")

        with open(self.storage_file, "r") as storage:
            self.results = format_function(storage.read())


data = TraceRoutes(results=[], storage_file="results.yaml")
data.from_file()
data.to_file()
print(data.results)
data.storage_file = "results.json"
data.to_file()
data.from_file()
print(data.results)
