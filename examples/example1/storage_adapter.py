#!/usr/bin/python3

import os
import json
import yaml

DATA = {
    "VALUE": 1,
    "THING": ["a", "b"]
}

def magic_json(json_data):
    return json_data


def magic_yaml(yaml_data):
    return yaml_data



class StorageType:
    def get(self, name):
        raise NotImplementedError("Implement this")
    def put(self):
        raise NotImplementedError("Implement this")
    def new(self, name):
        raise NotImplementedError("Implement this")


class YamlStorage(StorageType):
    def new(self, name):
        with open(name, 'w') as new_store:
            new_store.write(
                yaml.dump({})
            )
    def put(self, name, data):
        with open(name, 'w') as store:
            store.write(
                yaml.dump(data)
            )


class JsonStorage(StorageType):
    def new(self, name):
        with open(name, 'w') as new_store:
            new_store.write(
                json.dumps({})
            )
    def put(self, name, data):
        with open(name, 'w') as store:
            store.write(
                json.dumps(data)
            )


def init_storage():
    storage_types = {
        "json": JsonStorage,
        "yaml": YamlStorage
    }
    storage_class = storage_types[
        os.environ.get("STORAGETYPE")
    ]
    return storage_class()


def main():
    storage = init_storage()
    something_id = 1
    storage.new(f"./data_storage{something_id}")

main()
