FROM python:3-slim

WORKDIR /app
COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY passwordbot1.py passwordbot1.py

CMD python /app/passwordbot1.py
