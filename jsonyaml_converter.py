#!/usr/bin/python3

import os
import json
import yaml

DATA = {
    "VALUE": 1,
    "THING": ["a", "b"]
}


def magic_json(json_data):
    return json_data


def magic_yaml(yaml_data):
    return yaml_data


class StorageType:
    def get(self, name):
        raise NotImplementedError("Implement this")

    def put(self, chat_id, service_name, login, password):
        raise NotImplementedError("Implement this")

    def new(self):
        raise NotImplementedError("Implement this")

    def delete(self, chat_id, pwd_id):
        raise NotImplementedError("Implement this")

    def delete_all(self, chat_id):
        raise NotImplementedError("Implement this")


class YamlStorage(StorageType):
    def put(self, chat_id, service_name='None', login='None', password='None'):
        with open(f"userdata.yaml", 'r') as d:
            old_data = yaml.safe_load(d)
        usr_data = {'service_name': service_name, 'login': login, 'password': password}
        with open(f"userdata.yaml", 'w') as new_data:
            if f'chatid_{chat_id}' in old_data:
                key_list = list()
                for key in old_data[f'chatid_{chat_id}']:
                    key_list.append(key)
                print(key_list)
                old_data[f'chatid_{chat_id}'].update({(key_list[-1] + 1): usr_data})
            else:
                key_list = []
                for key in old_data:
                    key_list.append(key)
                old_data.update({f'chatid_{chat_id}': {1: usr_data}})
            yaml.safe_dump(old_data, new_data, default_flow_style=False)

    def new(self):
        if not os.path.exists('userdata.yaml'):
            with open(f"userdata.yaml", 'w') as new_data:
                yaml_data = {'chatid_0': {0: {'SN': 'sn', 'L': 'l', 'P': 'p'}}}
                yaml.safe_dump(yaml_data, new_data, default_flow_style=False)
        else:
            pass

    def get(self, chat_id):
        with open(f"userdata.yaml", 'r') as data:
            data_lines = yaml.load(data, Loader=yaml.FullLoader)
        if f'chatid_{chat_id}' in data_lines:
            return data_lines[f'chatid_{chat_id}']
        else:
            return None

    def delete(self, chat_id, pwd_id):
        with open(f"userdata.yaml", 'r') as d:
            old_data = yaml.safe_load(d)
        with open(f"userdata.yaml", 'w') as new_data:
            if int(pwd_id) in old_data[f'chatid_{chat_id}']:
                old_data[f'chatid_{chat_id}'].pop(int(pwd_id))
                removed2 = old_data.pop(f'chatid_{chat_id}')
                values = list(removed2.values())
                newdata = {}
                pwdid = 1
                for item in values:
                    newdata.update({pwdid: item})
                    pwdid += 1
                old_data.update({f'chatid_{chat_id}': newdata})
                yaml.safe_dump(old_data, new_data, default_flow_style=False)
            else:
                pass

    def delete_all(self, chat_id):
        with open(f"userdata.yaml", 'r') as d:
            old_data = yaml.safe_load(d)
            old_data.pop(f'chatid_{chat_id}')
        with open(f"userdata.yaml", 'w') as new_data:
            yaml.safe_dump(old_data, new_data, default_flow_style=False)


class JsonStorage(StorageType):
    def new(self):
        if not os.path.exists('userdata.json'):
            with open('userdata.json', 'w') as new_store:
                new_store.write(
                    json.dumps(
                        {'chatid_0': {'0': {'SN': 'sn', 'L': 'l', 'P': 'p'}}}
                    )
                )
        else:
            pass

    def put(self, chat_id, service_name='None', login='None', password='None'):
        with open('userdata.json', 'r') as data:
            old_data = json.load(data)
        usr_data = {'service_name': service_name, 'login': login, 'password': password}
        with open('userdata.json', 'w') as new_store:
            if f'chatid_{chat_id}' in old_data:
                key_list = []
                for key in old_data[f'chatid_{chat_id}']:
                    key_list.append(int(key))
                old_data[f'chatid_{chat_id}'].update({(key_list[-1] + 1): usr_data})
            else:
                key_list = []
                for key in old_data:
                    key_list.append(key)
                old_data.update({f'chatid_{chat_id}': {1: usr_data}})
            new_store.write(json.dumps(old_data))

    def get(self, chat_id):
        with open('userdata.json', 'r') as data:
            usrdata = json.load(data)
        if f'chatid_{chat_id}' in usrdata:
            return usrdata[f'chatid_{chat_id}']
        else:
            return None

    def delete(self, chat_id, pwd_id):
        with open('userdata.json', 'r') as data:
            old_data = json.load(data)
        with open(f"userdata.json", 'w') as new_data:
            if str(pwd_id) in old_data[f'chatid_{chat_id}']:
                old_data[f'chatid_{chat_id}'].pop(str(pwd_id))
                removed = old_data.pop(f'chatid_{chat_id}')
                values = list(removed.values())
                newdata = {}
                pwdid = 1
                for item in values:
                    newdata.update({pwdid: item})
                    pwdid += 1
                old_data.update({f'chatid_{chat_id}': newdata})
                new_data.write(json.dumps(old_data))
            else:
                pass

    def delete_all(self, chat_id):
        with open('userdata.json', 'r') as data:
            old_data = json.load(data)
        with open(f"userdata.json", 'w') as new_data:
            old_data.pop(f'chatid_{chat_id}')
            new_data.write(json.dumps(old_data))


def init_storage():
    storage_types = {
        "json": JsonStorage,
        "yaml": YamlStorage
    }
    storage_class = storage_types[
        config()['storagetype']
    ]
    return storage_class()


def main():
    storage = init_storage()
    print(storage)
    storage.new()


def config():
    with open('config.json', 'r') as con:
        return json.load(con)


#print(config())
#test = YamlStorage()
#test.new()
#test.put(chat_id='112', service_name='test3')
#test.put(chat_id='112', service_name='test2')
#values = test.get('112')
#print(values)
